AutoForce
=========


AutoForce Works in Threes
-------------------------

#) Families

#) Pipelines

#) Reports


Families
--------
Families are composed of patients. Some patients are affected, and some patients have been sequenced.


Pipelines
---------
AutoForce should support multiple pipelines, as the pipeline might change as time passes.

Reports
-------
A main goal of AutoForce is to auto-generate a write-up of results.
