from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Antibiotic

class AntibioticListView(ListView):
    model=Antibiotic
    context_object_name='antibiotic_list'

class AntibioticDetailView(DetailView):
    model=Antibiotic
    context_object_name='antibiotic'
