from django.conf.urls import include, patterns, url

from .views import AntibioticDetailView, AntibioticListView

urlpatterns=patterns('',
                     url(r'^$',AntibioticListView.as_view(),name='antibiotic-list'),
                     url(r'^(?P<antibiotic_pk>\d+)/$',AntibioticDetailView.as_view(),name='antibiotic-detail'),
                     )
