from django.test import TestCase
from django.core.urlresolvers import reverse

from patient.models import Ethnicity, Gender, Patient
from family.models import Family

def create_simple_patient(patient_alias,family):
    return Patient.objects.create(alias=patient_alias,family=family)

def create_complex_patient(patient_alias,family,gender=None,yob=None,living=None,affected=None,mother=None,father=None):
    return Patient.objects.create(alias=patient_alias,
                                  gender=gender,
                                  yearofbirth=yob+'-01-01', #everyone is born on Jan1
                                  mother=mother,father=father,living=living,affected=affected,family=family)


class PatientTests(TestCase):
    def test_no_patients(self):
        self.assertEqual(len(Patient.objects.all()),0)

    def test_one_patient(self):
        family1=Family.objects.create(alias='DM001')
        patient1=create_simple_patient(patient_alias='0001',family=family1)
        self.assertEqual(len(Patient.objects.all()),1)
        self.assertEqual(str(patient1),'0001')

    def test_two_patients(self):
        family1=Family.objects.create(alias='DM001')
        patient1=create_simple_patient(patient_alias='0001',family=family1)
        patient2=create_simple_patient(patient_alias='1000',family=family1)
        self.assertEqual(len(Patient.objects.all()),2)
        self.assertEqual(str(patient1),'0001')
        self.assertEqual(str(patient2),'1000')

    def test_one_trio(self):
        gender_male=Gender.objects.create(gender='male')
        gender_female=Gender.objects.create(gender='female')
        family1=Family.objects.create(alias='DM001')
        proband=create_complex_patient(patient_alias='0001',gender=gender_male,yob='2013',living=True,affected=True,family=family1)
        mother=create_complex_patient(patient_alias='1000',gender=gender_female,yob='1983',living=True,affected=False,family=family1)
        father=create_complex_patient(patient_alias='1001',gender=gender_male,yob='1982',living=True,affected=False,family=family1)
        self.assertEqual(proband.mother,None)
        self.assertEqual(proband.father,None)
        proband.mother=mother
        proband.father=father
        self.assertEqual(proband.mother,mother)
        self.assertEqual(proband.father,father)


class PatientViewTests(TestCase):
    def test_index_view_with_no_patients(self):
        """
        If no patients exist, an appropriate message should be displayed
        """
        pass
        #response=self.client.get(reverse('index'))
        #self.assertEqual(response.status_code,200)
        #self.assertContains(response,'No patients are listed in the database for this family')
        #self.assertQuerysetEqual(response.context['patient_list'],list())

    def test_index_view_with_one_patient(self):
        pass

    def test_index_view_with_two_patients(self):
        pass

class PatientDetailTests(TestCase):
    pass
