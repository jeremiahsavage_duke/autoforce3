from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DeleteView, DetailView, CreateView, UpdateView

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


from .models import Patient
from .forms import GeneForm, PatientForm, PhenotypeForm
from family.models import Family
from pedigree.views import update_pedigree
from phenotype.models import Phenotype

class PatientListView(ListView):
    model=Patient


class PatientDetailView(DetailView):
    model=Patient
    pk_url_kwarg='patient_pk'

    def get_context_data(self,**kwargs):
        context=super(PatientDetailView,self).get_context_data(**kwargs)
        context['family']=Family.objects.get(pk=self.kwargs.get('family_pk'))
        patient=Patient.objects.get(pk=self.kwargs.get('patient_pk'))
        #phenotype_list=patient.phenotype_set.all()
        #context['phenotype_list']=phenotype_list
        return context

    def get_form_kwargs(self):
        kwargs=super(PatientUpdateView,self).get_form_kwargs()
        kwargs['family']=self.kwargs.get('family_pk')
        kwargs['patient']=self.kwargs.get('patient_pk')
        return kwargs


    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(PatientDetailView,self).dispatch(*args,**kwargs)


class PatientUpdateView(UpdateView):
    model=Patient
    form_class=PatientForm
    pk_url_kwarg='patient_pk'

    def get_success_url(self):
        return reverse('data:family:family-detail',args=[self.kwargs['family_pk']])

    def get_form_kwargs(self):
        kwargs=super(PatientUpdateView,self).get_form_kwargs()
        kwargs['family_pk']=self.kwargs.get('family_pk')
        kwargs['patient_pk']=self.kwargs.get('patient_pk')
        return kwargs

    def form_valid(self,form):
        family=self.object.family
        self.object=form.save()
        update_pedigree(family)
        return super(PatientUpdateView,self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(PatientUpdateView,self).dispatch(*args,**kwargs)


class PatientCreateView(CreateView):
    model=Patient
    form_class=PatientForm

    def get_initial(self, *args, **kwargs):
        family=get_object_or_404(Family,pk=self.kwargs.get('family_pk'))
        return {'family':family,}

    def get_context_data(self,**kwargs):
        context=super(PatientCreateView,self).get_context_data(**kwargs)
        context['family']=Family.objects.get(pk=self.kwargs.get('family_pk'))
        return context

    def get_form_kwargs(self):
        kwargs=super(PatientCreateView,self).get_form_kwargs()
        kwargs['family_pk']=self.kwargs.get('family_pk')
        kwargs['patient_pk']=None
        return kwargs

    def get_success_url(self):
        return reverse('data:family:family-detail',args=[self.kwargs['family_pk']])

    def form_valid(self,form):
        self.object=form.save()
        family=self.object.family
        update_pedigree(family)
        #update_pedigree(self.object.family)
        return super(PatientCreateView,self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(PatientCreateView,self).dispatch(*args,**kwargs)


class PatientDeleteView(DeleteView):
    model=Patient
    pk_url_kwarg='patient_pk'


    def delete(self,request,*args,**kwargs):
        self.object=self.get_object()
        family=self.object.family
        self.object.delete()
        update_pedigree(family)
        return HttpResponseRedirect(self.get_success_url())

        
    def get_success_url(self):
        return reverse('data:family:family-detail',args=[self.kwargs['family_pk']])

class PatientPhenotypeListView(ListView):
    model=Patient
    template_name='patient/phenotype_list.html'

    def get_queryset(self):
        queryset=super(PatientPhenotypeListView,self).get_queryset()
        return queryset

    def get_context_data(self,**kwargs):
        context=super(PatientPhenotypeListView,self).get_context_data(**kwargs)
        patient_pk=self.kwargs['patient_pk']
        patient=Patient.objects.get(pk=patient_pk)
        species=patient.family.species
        context['species']=species
        context['patient']=patient
        phenotype_list=patient.phenotype_set.all()
        context['phenotype_list']=phenotype_list
        return context

    def get_template_names(self):
        return self.template_name

class PatientPhenotypeUpdateView(UpdateView):
    model=Patient
    form_class=PhenotypeForm
    template_name='patient/phenotype_form.html'
    pk_url_kwarg='patient_pk'

    def get_context_data(self,**kwargs):
        context=super(PatientPhenotypeUpdateView,self).get_context_data(**kwargs)
        context['phenotype']=Phenotype.objects.all()
        return context

    
    def get_success_url(self):
        return reverse('data:family:patient:patient-detail',kwargs={'patient_pk':self.kwargs['patient_pk'] , 'family_pk':self.kwargs['family_pk']})


    def get_template_names(self):
        return self.template_name


class PatientPhenotypeDetailView(DetailView):
    model=Phenotype
    template_name='patient/phenotype_detail.html'
    pk_url_kwarg='phenotype_pk'

    def get_queryset(self):
        queryset=super(PatientPhenotypeDetailView,self).get_queryset()
        patient_pk=self.kwargs['patient_pk']
        patient=Patient.objects.get(pk=patient_pk)
        return queryset

    def get_template_names(self):
        return self.template_name


class PatientGeneUpdateView(UpdateView):
    model=Patient
    form_class=GeneForm
    template_name='patient/gene_form.html'
    pk_url_kwarg='patient_pk'


    def get_success_url(self):
        return reverse('data:family:patient:patient-detail',kwargs={'patient_pk':self.kwargs['patient_pk'], 'family_pk':self.kwargs['family_pk']})
    #def get_context_data(self,**kwargs):
    #    context=super(PatientGeneUpdateView,self).get_context_data(**kwargs)
    #    return context
