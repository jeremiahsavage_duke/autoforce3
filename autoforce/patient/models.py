from django.db import models

from person.models import Person
from family.models import Family
from gene.models import Gene
from phenotype.models import Phenotype

class Ethnicity(models.Model):
    ethnicity=models.CharField(max_length=100,blank=True,default=str())

    def __str__(self):
        return self.ethnicity


class Gender(models.Model):
    gender=models.CharField(max_length=10,blank=True,default=str())

    def __str__(self):
        return self.gender


class PatientManager(models.Manager):
    def get_by_natural_key(self,family,alias):
        return self.get(family=family,alias=alias)

# TODO: limit parent selection to proper gender
class Patient(Person):
    objects=PatientManager()
    #TODO - assert mother/father dob is < children dob
    alias=models.CharField(max_length=100)
    gender=models.ForeignKey(Gender,blank=True,null=True)
    ethnicity=models.ForeignKey(Ethnicity,blank=True,null=True)
    birthyear=models.IntegerField(blank=True,null=True)
    mother=models.ForeignKey('self',related_name='visitsAsMother',blank=True,null=True)
    father=models.ForeignKey('self',related_name='visitsAsFather',blank=True,null=True)
    living=models.NullBooleanField()
    affected=models.NullBooleanField()
    proband=models.NullBooleanField()
    family=models.ForeignKey(Family)
    phenotype=models.ManyToManyField(Phenotype)
    gene=models.ManyToManyField(Gene)

    def __str__(self):
        return self.alias

    class Meta:
        ordering=['alias']
        unique_together=('alias','family',)
        verbose_name='patient'
        verbose_name_plural='patients'


class TwinZygosity(models.Model):
    zygosity=models.CharField(max_length=255,blank=False)

    def __str__(self):
        return self.zygosity

#TODO: limit twin selection to children of at least one parent
class Twin(models.Model):
    twinA=models.ForeignKey(Patient,related_name='vistsAsTwinA',null=False)
    twinB=models.ForeignKey(Patient,related_name='vistsAsTwinB',null=False)
    zygosity=models.ForeignKey(TwinZygosity,null=False)
