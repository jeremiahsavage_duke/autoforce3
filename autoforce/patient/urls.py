from django.conf.urls import include, patterns, url

from .views import PatientCreateView, PatientDeleteView, PatientDetailView, PatientUpdateView, PatientPhenotypeDetailView, PatientPhenotypeListView, PatientPhenotypeUpdateView, PatientGeneUpdateView, PatientListView

urlpatterns=patterns('',
                     #url(r'^$',PatientIndexView.as_view(),name='patient-index'),
                     url(r'^create/$',PatientCreateView.as_view(),name='patient-create'),
                     url(r'^(?P<patient_pk>\d+)/$',PatientDetailView.as_view(),name='patient-detail'),
                     url(r'^(?P<patient_pk>\d+)/delete/$',PatientDeleteView.as_view(),name='patient-delete'),
                     url(r'^(?P<patient_pk>\d+)/update/$',PatientUpdateView.as_view(),name='patient-update'),
                     url(r'^(?P<patient_pk>\d+)/patientdata/',include('datafiles.urls',namespace='datafiles')),
                     url(r'^(?P<patient_pk>\d+)/phenotype/$',PatientPhenotypeListView.as_view(),name='phenotype-list'),
                     url(r'^(?P<patient_pk>\d+)/phenotype/(?P<phenotype_pk>\d+)/$',PatientPhenotypeDetailView.as_view(),name='phenotype-detail'),
                     url(r'^(?P<patient_pk>\d+)/edit_phenotype/$',PatientPhenotypeUpdateView.as_view(),name='phenotype-update'),
                     url(r'^(?P<patient_pk>\d+)/edit_gene/$',PatientGeneUpdateView.as_view(),name='gene-update'),
                     url(r'^patient_list/$',PatientListView.as_view(),name='patient-list'),
                     )














