from django.forms import ModelForm, HiddenInput, CheckboxSelectMultiple
from django import forms

from mptt.forms import TreeNodeMultipleChoiceField

from .models import Patient
from phenotype.models import Phenotype
from gene.models import Gene

class PatientForm(ModelForm):
    def __init__(self,family_pk,patient_pk,*args,**kwargs):
        super(PatientForm,self).__init__(*args,**kwargs)
        self.fields['mother'].queryset=Patient.objects.filter(family=family_pk).exclude(id=patient_pk)
        self.fields['father'].queryset=Patient.objects.filter(family=family_pk).exclude(id=patient_pk)
    class Meta:
        model=Patient
        fields='__all__'
        exclude=['phenotype','gene']
        widgets={'family':HiddenInput(),'phenotype':HiddenInput()}


class PhenotypeForm(ModelForm):
    #def __init__(self,*args,**kwargs):
    #    super(PhenotypeForm,self).__init__(*args,**kwargs)
    phenotype=TreeNodeMultipleChoiceField(queryset=Phenotype.objects.all(),
                                          widget=forms.CheckboxSelectMultiple,
                                      )
    class Meta:
        model=Patient
        fields=['phenotype']


class GeneForm(ModelForm):
    gene=forms.ModelMultipleChoiceField(Gene.objects.all(),
                                        widget=forms.CheckboxSelectMultiple,
                               )
    class Meta:
        model=Patient
        fields=['gene']
