from django.db import models

from family.models import Family
from workflow.models import Workflow

class Report(models.Model):
    family=models.ForeignKey(Family)
    workflow=models.ForeignKey(Workflow)
