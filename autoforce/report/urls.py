from django.conf.urls import include, patterns, url

from .views import ReportListView, ReportDetailView

urlpatterns=patterns('',
                     url(r'^$',ReportListView.as_view(),name='report-list'),
                     url(r'^(?P<report_pk>\d+)/$',ReportDetailView.as_view(),name='report-detail'),
                     #url(r'/humangenes/',include('humangenes.urls',namespace='humangenes')),
)
