from django.shortcuts import render
from django.views.generic import CreateView, DeleteView, DetailView, ListView

from .models import Report

class ReportListView(ListView):
    model=Report

class ReportDetailView(DetailView):
    model=Report
