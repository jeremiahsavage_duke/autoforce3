from django.shortcuts import render
from django.views.generic import ListView, DetailView

from .models import Workflow

class WorkflowListView(ListView):
    model=Workflow


class WorkflowDetailView(DetailView):
    model=Workflow
