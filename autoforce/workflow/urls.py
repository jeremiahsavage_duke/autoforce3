from django.conf.urls import include, patterns, url

from .views import WorkflowListView, WorkflowDetailView

urlpatterns=patterns('',
                     url(r'^$',WorkflowListView.as_view(),name='workflow-list'),
                     url(r'^(?P<workflow_pk>\d+)/$',WorkflowDetailView.as_view(),name='workflow-detail'),
                     #url(r'/humangenes/',include('humangenes.urls',namespace='humangenes')),
)
