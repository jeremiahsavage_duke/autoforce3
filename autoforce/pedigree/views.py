import csv
import errno
import os
import shutil
import subprocess
from time import sleep

from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from patient.models import Patient, Twin

def write_twins(family_twin_list,family):
    header='\tid1\tid2\tcode\n'
    fs=FileSystemStorage()
    family_dir=os.path.join(fs.location,family.alias)
    pedigree_dir=os.path.join(family_dir,'pedigree')
    try:
        os.makedirs(pedigree_dir)
    except OSError as exc:
        if exc.errno==errno.EEXIST and os.path.isdir(pedigree_dir):
            pass
        else:
            raise
    csv_outfile=os.path.join(pedigree_dir,'twins.ped')
    outfile_open=open(csv_outfile,'w')
    outfile_open.write(header)
    writer=csv.writer(outfile_open,delimiter='\t',quotechar='"')
    for idx,twins in enumerate(family_twin_list):
        #zygosity
        if str(twins.zygosity)=='monozygotic':
            zygosity=1
        if str(twins.zygosity)=='dizygotic':
            zygosity=2
        if str(twins.zygosity)=='unknown zygosity':
            zygosity=3
        else:
            pass
        #twinA
        twinA=twins.twinA
        twinB=twins.twinB
        writer.writerow([idx+1,twinA,twinB,zygosity])
    outfile_open.close()


def write_ped(patient_queryset,family):
    header='\tped\tid\tfather\tmother\tsex\taffect\tavail\tstatus\n'
    fs=FileSystemStorage()
    family_dir=os.path.join(fs.location,family.alias)
    pedigree_dir=os.path.join(family_dir,'pedigree')
    try:
        os.makedirs(pedigree_dir)
    except OSError as exc:
        if exc.errno==errno.EEXIST and os.path.isdir(pedigree_dir):
            pass
        else:
            raise
    csv_outfile=os.path.join(pedigree_dir,'pedigree.ped')
    outfile_open=open(csv_outfile,'w')
    outfile_open.write(header)
    writer=csv.writer(outfile_open,delimiter='\t',quotechar='"')
    for idx,patient in enumerate(patient_queryset):
        #gender
        if str(patient.gender)=='male':
            gender=1
        elif str(patient.gender)=='female':
            gender=2
        elif str(patient.gender)=='unknown':
            gender=3
        elif str(patient.gender)=='terminated':
            gender=4
        else:
            gender=3
        #father
        if patient.father is not None:
            father=patient.father
        else:
            father=0#'NA'
        #mother
        if patient.mother is not None:
            mother=patient.mother
        else:
            mother=0#'NA'
        #affected
        if patient.affected is not None:
            affected=int(patient.affected)
        else:
            affected=0#'NA'
        #proband
        if patient.proband is not None:
            proband=int(patient.proband)
        else:
            proband=0#'NA'
        #living/status
        if patient.living is not None:
            if patient.living:
                status=0
            else:
                status=1
        else:
            status=0
        writer.writerow([idx+1,1,patient.alias,father,mother,gender,affected,proband,status])
    outfile_open.close()


def image_status(family):
    STATUSFILE='plotted.r'
    fs=FileSystemStorage()
    family_dir=os.path.join(fs.location,family.alias)
    pedigree_dir=os.path.join(family_dir,'pedigree')
    status_file=os.path.join(pedigree_dir,STATUSFILE)
    if os.path.isfile(status_file):
        status_open=open(status_file,'r')
        status=status_open.readline()
        status_open.close()
        if 'True' in status:
            return True
        else:
            return False
    else:
        return False


def generate_pedigree_image(family):
    fs=FileSystemStorage()
    Rscript=os.path.join(settings.PROJECT_ROOT,'pedigree','rscript','kinship2_pedigree.R')
    family_name=family.alias
    workingdir=os.path.join(fs.location,family_name,'pedigree')
    subprocesscall='Rscript '+' '+Rscript+' '+workingdir    
    subprocess.Popen([subprocesscall],shell=True)


def create_pedigree_dir(family):
    fs=FileSystemStorage()
    family_dir=os.path.join(fs.location,family.object.alias)
    pedigree_dir=os.path.join(family_dir,'pedigree')
    os.makedirs(family_dir)
    os.makedirs(pedigree_dir)


def delete_pedigree_dir(family):
    fs=FileSystemStorage()
    family_dir=os.path.join(fs.location,family.object.alias)
    pedigree_dir=os.path.join(family_dir,'pedigree')
    shutil.rmtree(pedigree_dir)


def create_pedigree(family):
    create_pedigree_dir(family)


def delete_pedigree(family):
    delete_pedigree_dir(family)


def get_twin_list(patient_queryset):
    family_twins_set=set()
    for patient in patient_queryset:
        resultA=Twin.objects.filter(twinA=patient)
        resultB=Twin.objects.filter(twinB=patient)
        if resultA:
            for result in resultA:
                family_twins_set.add(result)
        if resultB:
            for result in resultB:
                family_twins_set.add(result)
    return family_twins_set
        
def update_pedigree(family):
    patient_queryset=Patient.objects.filter(family=family.id)
    write_ped(patient_queryset,family)
    family_twin_list=get_twin_list(patient_queryset)
    write_twins(family_twin_list,family)
    generate_pedigree_image(family)
    sleep(0.3)
    if image_status(family):
        pedigree_url=os.path.join(family.alias,'pedigree','pedigree.png')
    else:
        pedigree_url=None
    family.pedigree=pedigree_url
    family.save()


