from django.db import models

from person.models import Person

class Title(models.Model):
    title=models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Degree(models.Model):
    degree=models.CharField(max_length=100)

    def __str__(self):
        return self.degree


class Researcher(Person):
    title=models.ManyToManyField(Title,blank=True)
    degree=models.ManyToManyField(Degree,blank=True)
    firstname=models.CharField(max_length=100,blank=True)
    middlename=models.CharField(max_length=100,blank=True)
    lastname=models.CharField(max_length=100,blank=True)

    def __str__(self):
        name=str()
        if self.firstname is not None and len(self.firstname)>0:
            name+=self.firstname+' '
        if self.middlename is not None and len(self.middlename)>0:
            name+=self.middlename+' '
        if self.lastname is not None and len(self.lastname)>0:
            name+=self.lastname
        return name.strip()

