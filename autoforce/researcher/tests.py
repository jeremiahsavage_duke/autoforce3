from django.test import TestCase
from django.core.urlresolvers import reverse

from medrespro.models import Degree, MedResProfessional, Title

def create_medical_professional(title=None,degree=None,firstname=None,middlename=None,lastname=None):
    medpro_degree=Degree.objects.create(degree=degree)
    medpro_degree.save()
    medpro_title=Title.objects.create(title=title)
    medpro_title.save()
    medrespro=MedResProfessional.objects.create(firstname=firstname,middlename=middlename,lastname=lastname)
    medrespro.title.add(medpro_title)
    medrespro.degree.add(medpro_degree)
    return medrespro

class MedResProfessionalTests(TestCase):
    def test_no_medical_professional(self):
        self.assertEqual(len(MedResProfessional.objects.all()),0)
    def test_one_medical_professional(self):
        medpro1=create_medical_professional(degree='M.D.',title='Professor',firstname='Jonas',middlename='Edward',lastname='Salk')
        self.assertEqual(len(MedResProfessional.objects.all()),1)
        self.assertEqual(str(medpro1.title.get()),'Professor')
        self.assertEqual(str(medpro1.degree.get()),'M.D.')
        self.assertEqual(str(medpro1),'Jonas Edward Salk')

    def test_index_view_with_two_medical_professionals(self):
        medpro1=create_medical_professional(degree='M.D.',title='Professor',firstname='Jonas',middlename='Edward',lastname='Salk')
        medpro2=create_medical_professional(degree='Ph.D.',title='Professor',firstname='Thomas',middlename='Hunt',lastname='Morgan')
        self.assertEqual(len(MedResProfessional.objects.all()),2)
        self.assertEqual(str(medpro1.title.get()),'Professor')
        self.assertEqual(str(medpro1.degree.get()),'M.D.')
        self.assertEqual(str(medpro1),'Jonas Edward Salk')
        self.assertEqual(str(medpro2.title.get()),'Professor')
        self.assertEqual(str(medpro2.degree.get()),'Ph.D.')
        self.assertEqual(str(medpro2),'Thomas Hunt Morgan')


class MedResProfessionalViewTests(TestCase):
    def test_index_view_with_no_medical_professionals(self):
        pass
    def test_index_view_with_one_medical_professional(self):
        pass
    def test_index_view_with_two_medical_professionals(self):
        pass
