from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from .models import Researcher

class ResearcherListView(generic.ListView):
    queryset=Researcher.objects.order_by('lastname')


class ReseracherDetailView(generic.DetailView):
    model=Researcher
