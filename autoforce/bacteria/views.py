from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Bacteria

class BacteriaDetailView(DetailView):
    model=Bacteria
    context_object_name='bacteria'

class BacteriaListView(ListView):
    model=Bacteria
    context_object_name='bacteria_list'
