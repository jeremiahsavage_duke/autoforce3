from django.conf.urls import include, patterns, url

from .views import BacteriaDetailView, BacteriaListView

urlpatterns=patterns('',
                     url(r'^$',BacteriaListView.as_view(),name='bacteria-list'),
                     url(r'^(?P<antibiotic_pk>\d+)/$',BacteriaDetailView.as_view(),name='bacteria-detail'),
                     )
