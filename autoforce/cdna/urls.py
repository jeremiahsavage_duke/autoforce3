from django.conf.urls import include, patterns, url

from .views import CDNADetailView, CDNAListView

urlpatterns=patterns('',
                     url(r'^$',CDNAListView.as_view(),name='cdna-list'),
                     url(r'^(?P<cdna_pk>\d+)/$',CDNADetailView.as_view(),name='cdna-detail'),
                     )
