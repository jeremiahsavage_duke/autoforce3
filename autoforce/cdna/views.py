from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import CDNA

class CDNADetailView(DetailView):
    model=CDNA
    context_object_name='cdna'

class CDNAListView(ListView):
    model=CDNA
    context_object_name='cdna_list'
