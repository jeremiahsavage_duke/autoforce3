from django.db import models

from taxonomy.models import Species

class CDNA(models.Model):
    name=models.CharField(max_length=255,unique=True)
    species=models.ForeignKey(Species)
    #tissue=models.CharField(max_length=255) to ontology
