from django.conf.urls import include, patterns, url

from .views import DataTemplateView

urlpatterns=patterns('',
                     url(r'^$',DataTemplateView.as_view(),name='data'),
                     url(r'^taxonomy/',include('taxonomy.urls',namespace='taxonomy')),
                     url(r'^reagent/',include('reagent.urls',namespace='reagent')),
                     url(r'^family/',include('family.urls',namespace='family')),
                     url(r'^experiment/',include('experiment.urls',namespace='experiment')),
                     url(r'^researcher/',include('researcher.urls',namespace='researcher')),
                     url(r'^stock/',include('stock.urls',namespace='stock')),
                     url(r'^patient/',include('patient.urls',namespace='patient')),
                     )
