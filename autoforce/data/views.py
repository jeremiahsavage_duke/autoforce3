from django.shortcuts import render
from django.views.generic import TemplateView

class DataTemplateView(TemplateView):
    template_name='data/data_template.html'
