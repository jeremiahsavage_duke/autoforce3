from django.shortcuts import render, get_object_or_404

from django.views.generic import ListView, DetailView

from .models import Gene, Allele
from taxonomy.models import Species

class GeneListView(ListView):
    model=Gene
    template_name='gene/gene_list.html'
    
    def get_queryset(self):
        ncbi_species=get_object_or_404(Species,pk=self.kwargs['species_pk'])
        queryset=Gene.objects.filter(ncbi_species__id=ncbi_species.id)
        return queryset


class GeneDetailView(DetailView):
    model=Gene
    pk_url_kwarg='gene_pk'
