import csv
from operator import itemgetter

f=open('sed_hgnc_download.tsv','r')
data=csv.DictReader(f,delimiter='\t')
pk=0
entries=[]
for row in data:
    entry=()
    entry = entry + (row['hgnc_symbol'],)
    entry = entry + (row['hgnc_name'],)
    entries.append(entry)

entries.sort(key=itemgetter(0))

outfile=open('initial_data.csv','w')
for entry in entries:
    outfile.write(entry[0]+',"'+entry[1]+'"\n')
outfile.close()
f.close()
