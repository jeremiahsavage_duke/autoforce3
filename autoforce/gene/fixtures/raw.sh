a: http://www.genenames.org/cgi-bin/hgnc_downloads?col=gd_hgnc_id&col=gd_app_sym&col=gd_app_name&col=gd_pub_chrom_map&col=gd_pub_acc_ids&col=gd_pub_refseq_ids&col=md_eg_id&col=md_mim_id&col=md_refseq_id&col=md_prot_id&col=md_ensembl_id&status=Approved&status_opt=2&where=&order_by=gd_hgnc_id&format=text&limit=&hgnc_dbtag=on&submit=submit

b: http://www.genenames.org/cgi-bin/hgnc_downloads?col=gd_hgnc_id&col=gd_app_sym&col=gd_app_name&col=gd_pub_chrom_map&col=gd_pub_acc_ids&col=gd_pub_eg_id&col=gd_pub_ensembl_id&col=gd_pubmed_ids&col=gd_pub_refseq_ids&col=gd_primary_ids&col=md_mim_id&col=md_prot_id&status=Approved&status_opt=2&where=&order_by=gd_hgnc_id&format=text&limit=&hgnc_dbtag=on&submit=submit

c: http://www.genenames.org/cgi-bin/hgnc_downloads?col=gd_hgnc_id&col=gd_app_sym&col=gd_app_name&col=gd_pub_chrom_map&col=gd_pub_acc_ids&col=gd_pub_eg_id&col=gd_pub_ensembl_id&col=gd_pubmed_ids&col=gd_pub_refseq_ids&col=md_mim_id&col=md_prot_id&status=Approved&status_opt=2&where=&order_by=gd_hgnc_id&format=text&limit=&hgnc_dbtag=on&submit=submit

HGNC ID , hgnc_id
Approved Symbol , hgnc_symbol
Approved Name , hgnc_name
Chromosome, chromosome
Accession Numbers, ncbi_accession_number
RefSeq IDs, refseq_id
Entrez Gene ID , entrez_id
OMIM ID(supplied by NCBI) , omim_id
UniProt ID(supplied by UniProt) , uniprot_id
Ensembl Gene ID , ensembl_accession
Pubmed IDs , pubmed_ids

cp hgnc_download.tsv sed_hgnc_download.tsv
sed -i 's/HGNC ID/hgnc_id/' sed_hgnc_download.tsv
sed -i 's/Approved Symbol/hgnc_symbol/' sed_hgnc_download.tsv
sed -i 's/Approved Name/hgnc_name/' sed_hgnc_download.tsv
sed -i 's/Chromosome/chromosome/' sed_hgnc_download.tsv
sed -i 's/Accession Numbers/ncbi_accession_number/' sed_hgnc_download.tsv
sed -i 's/RefSeq IDs/refseq_id/' sed_hgnc_download.tsv
sed -i 's/Entrez Gene ID/entrez_id/' sed_hgnc_download.tsv
sed -i 's/OMIM ID(supplied by NCBI)/omim_id/' sed_hgnc_download.tsv
sed -i 's/Ensembl Gene ID/ensembl_accession/' sed_hgnc_download.tsv
sed -i 's/Pubmed IDs/pubmed_ids/' sed_hgnc_download.tsv
sed -i 's/UniProt ID(supplied by UniProt)/uniprot_id/' sed_hgnc_download.tsv
sed -i 's/HGNC://' sed_hgnc_download.tsv
python ./hgnc_csv_json.py



####2013-12-12
### MYSQL insert from localhost
#convert tsv to Gene.csv, then
mysqlimport ccp --fields-optionally-enclosed-by="\"" -u jeremiah -h 152.16.223.90 -c symbol,name --fields-terminated-by="," --local Gene.csv  -p

#2014-01-16
mysqldump --databases ccp -p > out2.sql
mysql -u jeremiah -p -h localhost ccp < out.sql
