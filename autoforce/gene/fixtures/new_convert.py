import csv
import json

f=open('sed_hgnc_download.tsv','r')
data=csv.DictReader(f,delimiter='\t')
pk=0
entries=[]
for row in data:
    row['ncbi_species']=1
    pk+=1
    entry={}
    entry['pk']=int(pk)
    entry['model']='gene.gene'
    entry['fields']=row
    entries.append(entry)

out = json.dumps(entries,indent=4)
outfile=open('initial_data.json','w')
outfile.write(out)
outfile.close()
f.close()
