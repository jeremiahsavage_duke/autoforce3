from django.conf.urls import include, patterns, url

from .views import GeneDetailView, GeneListView

urlpatterns=patterns('',
                     url(r'^$',GeneListView.as_view(),name='gene-list'),
                     url(r'^(?P<gene_pk>\d+)/$',GeneDetailView.as_view(),name='gene-detail'),
                     )
