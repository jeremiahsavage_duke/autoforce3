from django.db import models

from taxonomy.models import Species

class Gene(models.Model):
    hgnc_id=models.IntegerField(blank=True,db_index=True)
    hgnc_symbol=models.CharField(max_length=255,db_index=True)
    hgnc_name=models.CharField(max_length=255,db_index=True)
    chromosome=models.CharField(max_length=255,blank=True)
    ensembl_accession=models.CharField(max_length=255,blank=True)
    entrez_id=models.CharField(max_length=255,blank=True)
    omim_id=models.CharField(max_length=255,blank=True)
    refseq_id=models.CharField(max_length=255,blank=True)
    uniprot_id=models.CharField(max_length=255,blank=True)
    ncbi_accession_number=models.CharField(max_length=255,blank=True)
    pubmed_ids=models.CharField(max_length=255,blank=True)
    ncbi_species=models.ForeignKey(Species,db_index=True)

    def __str__(self):
        return self.hgnc_symbol

    class Meta:
        ordering=['hgnc_symbol']
        unique_together=(('ncbi_species','hgnc_symbol'),)
        index_together=[['ncbi_species','hgnc_symbol'],]

class Allele(models.Model):
    gene=models.ForeignKey(Gene)
    position=models.IntegerField()
    reference=models.CharField(max_length=255)
    alternate=models.CharField(max_length=255)
