from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Experiment

class ExperimentDetailView(DetailView):
    model=Experiment

class ExperimentListView(ListView):
    model=Experiment
