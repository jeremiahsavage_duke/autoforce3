from django.conf.urls import include, patterns, url

from .views import ExperimentDetailView, ExperimentListView

urlpatterns=patterns('',
                     url(r'^$',ExperimentListView.as_view(),name='experiment-list'),
                     url(r'^(?P<experiment_pk>\d+)/$',ExperimentDetailView.as_view(),name='experiment-detail'),
                     url(r'nextgenseq/',include('experiment_nextgenseq.urls',namespace='experiment_nextgenseq')),
)
