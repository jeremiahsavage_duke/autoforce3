from django.db import models

from gene.models import Gene

class Antibody(models.Model):
    name=models.CharField(max_length=255,unique=True)
    monoclonal=models.NullBooleanField()
    target=models.ManyToManyField(Gene)
