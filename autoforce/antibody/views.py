from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Antibody

class AntibodyDetailView(DetailView):
    model=Antibody

class AntibodyListView(ListView):
    model=Antibody
