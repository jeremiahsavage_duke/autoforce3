from django.conf.urls import include, patterns, url

from .views import AntibodyDetailView, AntibodyListView

urlpatterns=patterns('',
                     url(r'^$',AntibodyListView.as_view(),name='antibody-list'),
                     url(r'^(?P<antibiotic_pk>\d+)/$',AntibodyDetailView.as_view(),name='antibody-detail'),
                     )
