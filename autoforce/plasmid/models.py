from django.db import models

from mptt.models import MPTTModel, TreeForeignKey

class Plasmid(models.Model):
    parent=TreeForeignKey('self',null=True,blank=True,related_name='children',db_index=True)
    name=models.CharField(max_length=255)
    def __str__(self):
        return self.name

class GenbankFile(models.Model):
    data=models.TextField()
    plasmid=models.ForeignKey(Plasmid)

class FastaSequence(models.Model):
    data=models.TextField()
    Plasmid=models.ForeignKey(Plasmid)


class AntibioticResistance(models.Model):
    antibioticresistance=models.CharField(max_length=255,unique=True)
    plasmid=models.ManyToManyField(Plasmid)

    def __str__(self):
        return self.antibioticresistance

class Source(models.Model):
    source=models.CharField(max_length=255)
    plasmid=models.ManyToManyField(Plasmid)

    def __str__(self):
        return self.source
