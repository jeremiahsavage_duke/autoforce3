import argparse
import csv
import fileinput
import os

parser=argparse.ArgumentParser('')
parser.add_argument('-d','--dirroot',required=True)
#parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

dirroot=args['dirroot']
#outfile=args['outfile']

def get_file_list(root):
    file_list=list()
    for root,dirs,files in os.walk(dirroot):
        for afile in files:
            if afile.endswith('.csv'):
                if afile.startswith('._'):
                    continue
                else:
                    file_list.append(str(root+'/'+afile))
    return file_list


def get_file_set(afile,colnum):
    data_set=set()
    with open(afile,'r') as afile_open:
        next(afile_open)
        for line in afile_open:
            newdata=line.split(',')[colnum].strip('\r\n').strip('\n').strip('"').strip().replace('\r',' ')
            data_set.add(newdata)
    return data_set
    

def get_set(file_list,colnum):
    datalist=list()
    for afile in file_list:
        newset=get_file_set(afile,colnum)
        for data in newset:
            if data is not '':
                datalist.append(data)
    dataset=sorted(set(datalist))
    return dataset

def is_plasmid_file(afile):
    with open(afile,'r') as afile_open:
        header=afile_open.readline()
    pos8=header.split(',')[8]
    return pos8=='vector'
        

def doit(dirroot):
    file_list=get_file_list(dirroot)


    for afile in file_list:
        if is_plasmid_file(afile):
            id_set=get_set(file_list,7) #ID
            vector_set=get_set(file_list,8) #vector
            resistance_set=get_set(file_list,9) #resistance
            origin_set=get_set(file_list,10) #origin
    
    idfile_open=open('id.dat','w')
    vectorfile_open=open('vector.dat','w')
    resistancefile_open=open('resistance.dat','w')
    originfile_open=open('origin.dat','w')
    #outfile_open=open(outfile,'w')
    #for file in file_list:
    #    outfile_open.write(file+'\n')
    for aid in id_set:
        idfile_open.write(str(aid)+'\n')
    for avector in vector_set:
        vectorfile_open.write(str(avector)+'\n')
    for aresistance in resistance_set:
        resistancefile_open.write(str(aresistance)+'\n')
    for aorigin in origin_set:
        originfile_open.write(str(aorigin)+'\n')

    idfile_open.close()
    vectorfile_open.close()
    resistancefile_open.close()
    originfile_open.close()
    
    

doit(dirroot)
