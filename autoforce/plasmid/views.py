from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Plasmid

class PlasmidDetailView(DetailView):
    model=Plasmid
    context_object_name='plasmid'

class PlasmidListView(ListView):
    model=Plasmid
    context_object_name='plasmid_list'
