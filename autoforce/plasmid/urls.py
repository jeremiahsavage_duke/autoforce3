from django.conf.urls import include, patterns, url

from .views import PlasmidDetailView, PlasmidListView

urlpatterns=patterns('',
                     url(r'^$',PlasmidListView.as_view(),name='plasmid-list'),
                     url(r'^(?P<plasmid_pk>\d+)/$',PlasmidDetailView.as_view(),name='plasmid-detail'),
                     )
