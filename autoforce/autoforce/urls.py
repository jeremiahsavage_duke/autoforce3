from django.conf.urls import include, patterns, url
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static


from django.contrib.auth.views import login, logout

#from filebrowser.sites import site

from .views import AutoforceAuthenticationForm
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', TemplateView.as_view(template_name='index.html'),name='autoforce-home'),
                       url(r'^data/',include('data.urls',namespace='data')),
                       url(r'^workflow/',include('workflow.urls',namespace='workflow')),
                       url(r'^report/',include('report.urls',namespace='report')),
                       url(r'^admin/', include(admin.site.urls)),
                       #url(r'^login/$',AutoforceAuthenticationForm.as_view(),name='autoforce-authentication'),
                       url(r'^accounts/login/$',login, name='accounts-login'),
                       url(r'^accounts/logout/$',logout, name='accounts-logout'),
                       #url(r'^accounts/profile/$',profile,name='accounts-profile'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
