"""Local test settings and globals which allows us to run our test suite locally."""

from autoforce.settings import *

### TEST SETTINGS
TEST_RUNNER='discover_runner.DiscoverRunner'
TEST_DISCOVER_TOP_LEVEL=PROJECT_ROOT
TEST_DISCOVER_ROOT=PROJECT_ROOT
TEST_DISCOVER_PATTERN="*"

### IN-MEMORY TEST DATABASE
DATABASES={
    'default': {
        'ENGINE':'django.db.backends.postgresql_psycopg2',
        'NAME': 'testdb1',
        'USER': 'jeremiah',
        'PASSWORD': 'asdfqwer12345',
        'HOST': 'localhost',
        'PORT': '',
    }
}
