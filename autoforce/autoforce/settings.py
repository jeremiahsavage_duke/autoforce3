"""
Django settings for autoforce project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
FAMILYDIRECTORY='/home/jeremiah/penguin/home/Erica/Baylor_Exome trio data'
APPS=['datafiles','family','pedigree','person','patient','researcher','workflow','report','gene','data','reagent','stock','phenotype','taxonomy','plasmid','primer','antibiotic','antibody','bacteria','cdna','experiment','experimentnextgenseq']
BASE_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
APP_FIXTURES=list(map(lambda x:os.path.join(PROJECT_ROOT,x,'fixtures'),APPS))
FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, 'fixtures'),
] + APP_FIXTURES
MEDIA_ROOT = os.path.join(BASE_DIR, 'site_media', 'media')
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(BASE_DIR, 'site_media', 'static')
TEMPLATE_DIRS = [os.path.join(BASE_DIR,'templates')]
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'r3s51p-*%i*a0n=n&afqt1_z3--$99^jw)7m_i6=b@c)*3zvx='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    #'crispy_forms',
    'bootstrap_toolkit',
    #'easy_thumbnails',
    'mptt',
    #'debug_toolbar',
    #'polymorphic',
    #'filer',
    #'PIL',

#    tuple(APPS),
    'datafiles',
    'family',
    'pedigree',
    'person',
    'patient',
    'researcher',
    'gene',
    'filternextgen',
    'workflow',
    'report',
    'experiment',
    'reagent',
    'stock',
    'phenotype',
    'data',
    'taxonomy',
    'plasmid',
    'antibiotic',
    'antibody',
    'bacteria',
    'morpholino',
    'primer',
    'cdna',
    'experimentnextgenseq',
)

MIDDLEWARE_CLASSES = (
    #'django_cprofile_middleware.middleware.ProfilerMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'autoforce.urls'

WSGI_APPLICATION = 'autoforce.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'testdb',
        'USER': 'jeremiah',
        'PASSWORD': 'asdfqwer12345',
        'HOST': 'localhost',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'
LOGIN_REDIRECT_URL='/'
'''
DEFAULT_FROM_EMAIL='jeremiahsavage.duke@gmail.com'
EMAIL_HOST='smtp.gmail.com'
EMAIL_HOST_PASSWORD='This!$apassw0rd'
EMAIL_HOST_USER='jeremiahsavage.duke'
EMAIL_PORT=465
EMAIL_USE_SSL=True
'''
DEFAULT_FROM_EMAIL='chdmnotice@gmail.com'
EMAIL_HOST='smtp.gmail.com'
EMAIL_HOST_PASSWORD='This!$apassw0rd'
EMAIL_HOST_USER='chdmnotice'
EMAIL_PORT=465
EMAIL_USE_TLS=True

#INTERNAL_IPS='152.16.223.125'
