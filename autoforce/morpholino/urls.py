from django.conf.urls import include, patterns, url

from .views import MorpholinoDetailView, MorpholinoListView

urlpatterns=patterns('',
                     url(r'^$',MorpholinoListView.as_view(),name='morpholino-list'),
                     url(r'^(?P<morpholino_pk>\d+)/$',MorpholinoDetailView.as_view(),name='morpholino-detail'),
                     )
