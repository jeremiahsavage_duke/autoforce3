from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Morpholino

class MorpholinoDetailView(DetailView):
    model=Morpholino
    context_object_name='morpholino'

class MorpholinoListView(ListView):
    model=Morpholino
    context_object_name='morpholino_list'
