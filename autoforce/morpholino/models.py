from django.db import models

class Morpholino(models.Model):
    name=models.CharField(max_length=255,unique=True)
    sequence=models.CharField(max_length=255)
