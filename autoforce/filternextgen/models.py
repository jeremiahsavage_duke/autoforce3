from django.db import models

from gene.models import Allele

class Indel(models.Model):
    candidate_indel=models.ForeignKey(Allele)

class SNP(models.Model):
    candidate_snp=models.ForeignKey(Allele)

class CompoundHet(models.Model):
    allele_a=models.ForeignKey(Allele,related_name='allele_a')
    allele_b=models.ForeignKey(Allele,related_name='allele_b')
