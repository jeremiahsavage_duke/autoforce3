from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Primer

class PrimerDetailView(DetailView):
    model=Primer
    context_object_name='primer'

class PrimerListView(ListView):
    model=Primer
    context_object_name='primer_list'
