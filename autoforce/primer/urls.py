from django.conf.urls import include, patterns, url

from .views import PrimerDetailView, PrimerListView

urlpatterns=patterns('',
                     url(r'^$',PrimerListView.as_view(),name='primer-list'),
                     url(r'^(?P<plasmid_pk>\d+)/$',PrimerDetailView.as_view(),name='primer-detail'),
                     )
