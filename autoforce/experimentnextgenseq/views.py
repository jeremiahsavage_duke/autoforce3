import os
from django.shortcuts import render
from django.views.generic import DetailView, ListView
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from .models import FamilySequenceData, PatientSequenceData

class FamilySequenceDataDetailView(DetailView):
    model=FamilySequenceData

class FamilySequenceDataListView(ListView):
    model=FamilySequenceData

class PatientSequenceDataDetailView(DetailView):
    model=PatientSequenceData

class PatientSequenceDataListView(ListView):
    model=PatientSequenceData

class Data(object):
    def __init__(self,**kwargs):
        fs=FileSystemStorage()
        self.mediadir=fs.location
        self.familysourcedir=settings.FAMILYDIRECTORY

    def getdirset(self,adir):
        dirset=set()
        filelist=os.listdir(adir)
        for afile in filelist:
            if os.path.isdir(os.path.join(adir,afile)):
                dirset.add(afile)
        return dirset

    def updatefilesystem(self):
        sourcedirset=self.getdirset(self.familysourcedir)
        mediadirset=self.getdirset(self.mediadir)
        unmadedirset=sourcedirset-mediadirset
        for unmadedir in unmadedirset:
            sourcedir=os.path.join(self.familysourcedir,unmadedir)
            newdir=os.path.join(self.mediadir,unmadedir)
            symdir=os.path.join(newdir,'datafiles')
            if not os.path.exists(newdir):
                os.makedirs(newdir)
            if not os.path.islink(symdir):
                os.symlink(sourcedir,symdir)
        for mediadir in mediadirset:
            symdir=os.path.join(self.mediadir,mediadir,'datafiles')
            sourcedir=os.path.join(self.familysourcedir,mediadir)
            if not os.path.islink(symdir):
                os.symlink(sourcedir,symdir)
            
                
    def addseqfilestodb(self):
        pass
