from django.conf.urls import include, patterns, url

from .views import FamilySequenceDataDetailView, FamilySequenceDataListView, PatientSequenceDataDetailView, PatientSequenceDataListView

urlpatterns=patterns('',
                     url(r'^$',FamilySequenceDataListView.as_view(),name='list'),
                     url(r'^(?P<nextgenseq_pk>\d+)/$',FamilySequenceDataDetailView.as_view(),name='detail'),
                 )
