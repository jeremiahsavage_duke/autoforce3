from django.db import models

from family.models import Family
from patient.models import Patient
from datafiles.models import DataFile

class FamilySequenceData(models.Model):
    family=models.ForeignKey(Family)
    fastqfile=models.ManyToManyField(DataFile,blank=True,related_name='family_fastqfile')
    bamfile=models.ManyToManyField(DataFile,blank=True,related_name='family_bamfile')
    bamindexfile=models.ManyToManyField(DataFile,blank=True,related_name='family_bamindexfile')
    vcffile=models.ManyToManyField(DataFile,blank=True,related_name='family_vcffile')
    xlsfile=models.ManyToManyField(DataFile,blank=True,related_name='family_xlsfile')

class PatientSequenceData(models.Model):
    patient=models.ForeignKey(Patient)
    fastqfile=models.ManyToManyField(DataFile,blank=True,related_name='patient_fastqfile')
    bamfile=models.ManyToManyField(DataFile,blank=True,related_name='patient_bamfile')
    bamindexfile=models.ManyToManyField(DataFile,blank=True,related_name='patient_bamindexfile')
    vcffile=models.ManyToManyField(DataFile,blank=True,related_name='patient_vcffile')
    xlsfile=models.ManyToManyField(DataFile,blank=True,related_name='patient_xlsfile')
