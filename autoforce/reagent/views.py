from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Reagent

class ReagentDetailView(DetailView):
    model=Reagent
    context_object_name='reagent'

class ReagentListView(ListView):
    model=Reagent
    context_object_name='reagent_list'
