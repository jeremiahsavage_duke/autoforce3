from django.conf.urls import include, patterns, url

from .views import ReagentDetailView, ReagentListView

urlpatterns=patterns('',
                     url(r'^$',ReagentListView.as_view(),name='reagent-list'),
                     url(r'^(?P<reagent_pk>\d+)/$',ReagentDetailView.as_view(),name='reagent-detail'),
                     url(r'^antibiotic/',include('antibiotic.urls',namespace='antibiotic')),
                     url(r'^antibody/',include('antibody.urls',namespace='antibody')),
                     url(r'^bacteria/',include('bacteria.urls',namespace='bacteria')),
                     url(r'^morpholino/',include('morpholino.urls',namespace='morpholino')),
                     url(r'^plasmid/',include('plasmid.urls',namespace='plasmid')),
                     url(r'^primer/',include('primer.urls',namespace='primer')),
                     url(r'^antibiotic/',include('antibiotic.urls',namespace='antibiotic')),
                     url(r'^cdna/',include('cdna.urls',namespace='cdna')),
                     )
