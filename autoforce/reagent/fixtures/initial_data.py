from reagent.models import Reagent

r1=Reagent.objects.create(type='antibiotic')
r2=Reagent.objects.create(type='antibody')
r3=Reagent.objects.create(type='bacteria')
r4=Reagent.objects.create(type='morpholino')
r5=Reagent.objects.create(type='plasmid')
r6=Reagent.objects.create(type='primer')
