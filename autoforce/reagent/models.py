from django.db import models

class Reagent(models.Model):
    type=models.CharField(max_length=255,unique=True)

    def __str__(self):
        return self.type
