from django.forms import ModelForm
#from django.core.files.storage import FileSystemStorage

from .models import Family

class FamilyForm(ModelForm):
    #def __init__(self,*args,**kwargs):
        #super(FamilyForm,self).__init__(*args,**kwargs)
        #fs=FileSystemStorage(location='/home/jeremiah/autoforce_project/autoforce/autoforce/site_media/media')
    class Meta:
        model=Family
        fields='__all__'
