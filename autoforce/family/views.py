from django.shortcuts import get_object_or_404, render
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy

from .models import Family
from .forms import FamilyForm
from patient.models import Patient
from pedigree.views import create_pedigree, delete_pedigree

##user select_for_update(nowait=True) and then provide helpful errror_message (catch: DatabaseError)
## get() can raise: MultipleObjectsReturned and DoesNotExist (ObjectDoesNotExist)
## consider Family.objects.get(id__exact=14),, Family.objects.get(name__iexact='dm001')
## consider Family.objects.get(name__icontains='dm')
## consider Family.objects.filter(name__gte='dm100')
## consider Family.objects.filter(name__lte='dm100')
## consider Family.objects.filter(name__istartswith='dm11')
## consider Family.objects.filter(name__iendswith='9)
## consider Family.objects.filter(name__range=('dm100','dm200'))
## consider Family.objects.filter(patients__gte=4)
## consider Family.objects.get_context_data(

## from django.core.exceptions import ObjectDoesNotExist
##except ObjectDoesNotExist:
##    print('You got no data')
## Family.objects.create() == p=Person() then p.save()
## bulk_create() for csv upload
## try test_s off quersets exists()
## use update() when editing fields
## use delete() to remove object(s).. warn family->all patients->all results (just hide data)


class FamilyListView(ListView):
    context_object_name='family_list'
    model=Family

    def get_queryset(self):
        return Family.objects.order_by('-alias')

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(FamilyListView,self).dispatch(*args,**kwargs)


class FamilyDetailView(DetailView):
    context_object_name='family'
    model=Family
    pk_url_kwarg='family_pk'

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(FamilyDetailView,self).dispatch(*args,**kwargs)


class FamilyCreateView(CreateView):
    model=Family
    pk_url_kwarg='family_pk'
    form_class=FamilyForm

    def get_success_url(self,*args,**kwargs):
        return reverse('data:family:family-detail',kwargs={'family_pk':self.object.pk})

    def form_valid(self,form):
        self.object=form.save()
        create_pedigree(self)
        return super(FamilyCreateView,self).form_valid(form)

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(FamilyCreateView,self).dispatch(*args,**kwargs)


class FamilyDeleteView(DeleteView):
    model=Family
    pk_url_kwarg='family_pk'
    success_url=reverse_lazy('data:family:family-list')

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(FamilyDeleteView,self).dispatch(*args,**kwargs)

class FamilyUpdateView(UpdateView):
    model=Family
    form_class=FamilyForm
    pk_url_kwarg='family_pk'

    def get_success_url(self,*args,**kwargs):
        return reverse('data:family:family-detail',kwargs={'family_pk':self.object.pk})

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(FamilyUpdateView,self).dispatch(*args,**kwargs)
