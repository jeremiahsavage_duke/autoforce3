from django.test import TestCase
from django.core.urlresolvers import reverse, NoReverseMatch

from family.models import Family
from patient.models import Patient

##TODO##
#fixtures=['a_fixture_file.json']
##/TODO##


class FamilyTest(TestCase):
    def test_no_families(self):
        self.assertEqual(len(Family.objects.all()),0)

    def test_one_family(self):
        family=Family(alias='DM001')
        family.save()
        self.assertEqual(len(Family.objects.all()),1)
        self.assertEqual(len(family.patient_set.all()),0)

    def test_two_families(self):
        family1=Family(alias='DM001')
        family2=Family(alias='DM002')
        family1.save()
        family2.save()
        self.assertEqual(len(Family.objects.all()),2)
        self.assertEqual(len(family1.patient_set.all()),0)
        patient1=Patient.objects.create(alias='0001',family=family1)
        self.assertEqual(len(family1.patient_set.all()),1)


class FamilyIndexTests(TestCase):
    def test_index_view_with_no_families(self):
        """
        If no families exist, an appropriate message should be displayed
        """
        response=self.client.get(reverse('family:index'))
        self.assertEqual(response.status_code,200)
        self.assertContains(response,'No families are in the database')
        self.assertQuerysetEqual(response.context['family_list'],list())

    def test_index_view_with_one_family(self):
        """
        If one family exists, it should be listed in singular
        """
        family1=Family.objects.create(alias='DM001')
        family1.save()
        response=self.client.get(reverse('family:index'))
        self.assertEqual(response.status_code,200)
        self.assertContains(response,'Family')
        self.assertEqual(len(response.context['family_list']),1)
        self.assertEqual([family.alias for family in response.context['family_list']],['DM001'])
        #self.assertEqual([family.id for family in response.context['family_list']],[1])
        #consider assertQuerysetEqual()

    def test_index_view_with_two_families(self):
        """
        If two families exists, they should be listed in plural
        """
        family1=Family.objects.create(alias='DM001')
        family2=Family.objects.create(alias='DM002')
        family1.save()
        family2.save()
        response=self.client.get(reverse('family:index'))
        self.assertEqual(response.status_code,200)
        self.assertContains(response,'Families')
        self.assertEqual(len(response.context['family_list']),2)


class FamilyDetailTests(TestCase):
    def test_detail_view_with_family_does_not_exist(self):
        """
        If family does not exist, perhaps a helpful 404 page
        """
        try:
            response=self.client.get(reverse('family:detail',kwargs={'family_id':1}))
        except NoReverseMatch:
            pass
        #self.assertEqual(response.status_code,404)

    def test_detail_view_with_a_empty_families(self):
        family1=Family.objects.create(alias='DM001')
        family1.save()
        response=self.client.get(reverse('family:detail',args=[family1.id]))
        self.assertContains(response,'DM001')
        isfam=Family.objects.get(alias='DM001')
        self.assertEqual(isfam.id,family1.id)

    def test_detail_view_with_a_single_person_family(self):
        family1=Family.objects.create(alias='DM001')
        patient1=Patient.objects.create(family=family1)
        pass
        #self.assertEqual(response.context['family'].pk,1)
        #self.assertContains(response,'Member:')
        #member=family.choice_set.all()
        #self.assertEqual(len(member),1)
        

    def test_detail_view_with_a_three_person_family(self):
        pass
        #family_members=family.choice_set.all()
        #assert affected,
