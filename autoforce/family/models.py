from django.db import models
from django.forms import ModelForm
from django.core.files.storage import FileSystemStorage

from taxonomy.models import Species

class FamilyManager(models.Manager):
    def get_by_natural_key(self, alias):
        return self.get(alias=alias)


class Family(models.Model):
    fs=FileSystemStorage()
    objects=FamilyManager()
    alias=models.CharField(max_length=100,unique=True)
    context_object_name='family'
    pedigree=models.ImageField(max_length=255,upload_to=fs,editable=False,blank=True,null=True)
    species=models.ForeignKey(Species)

    def __str__(self):
        return self.alias

    class Meta:
        ordering=['alias']
        verbose_name='family'
        verbose_name_plural='families'
