from django.conf.urls import include, patterns, url

from .views import FamilyCreateView, FamilyDeleteView, FamilyDetailView, FamilyListView, FamilyUpdateView

urlpatterns=patterns('',
                     url(r'^$',FamilyListView.as_view(),name='family-list'),
                     url(r'^create/$',FamilyCreateView.as_view(),name='family-create'),
                     url(r'^(?P<family_pk>\d+)/$',FamilyDetailView.as_view(),name='family-detail'),
                     url(r'^(?P<family_pk>\d+)/delete/$',FamilyDeleteView.as_view(),name='family-delete'),
                     #url(r'^(?P<family_pk>\d+)/update/$',FamilyUpdateView.as_view(),name='family-update'),
                     url(r'^(?P<family_pk>\d+)/patient/',include('patient.urls',namespace='patient')),
)
