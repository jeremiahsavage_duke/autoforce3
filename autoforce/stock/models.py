from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

class Freezer(MPTTModel):
    number=models.IntegerField()
    class MPTTMeta:
        order_insertion_by=['number']
    def __str__(self):
        return self.number

class Shelf(MPTTModel):
    number=models.IntegerField()
    parent=TreeForeignKey(Freezer,null=True,blank=True,related_name='children',db_index=True)
    class MPTTMeta:
        order_insertion_by=['number']
    def __str__(self):
        return self.number

class Rack(MPTTModel):
    number=models.IntegerField()
    parent=TreeForeignKey(Shelf,null=True,blank=True,related_name='children',db_index=True)
    class MPTTMeta:
        order_insertion_by=['number']
    def __str__(self):
        return self.number

class Drawer(MPTTModel):
    number=models.IntegerField()
    parent=TreeForeignKey(Rack,null=True,blank=True,related_name='children',db_index=True)
    class MPTTMeta:
        order_insertion_by=['number']
    def __str__(self):
        return self.number

class Box(MPTTModel):
    number=models.IntegerField()
    parent=TreeForeignKey(Drawer,null=True,blank=True,related_name='children',db_index=True)
    class MPTTMeta:
        order_insertion_by=['number']
    def __str__(self):
        return self.number


class Cell(MPTTModel):
    number=models.IntegerField()
    parent=TreeForeignKey(Box,null=True,blank=True,related_name='children',db_index=True)
    
    class MPTTMeta:
        order_insertion_by=['number']

    def __str__(self):
        return self.number
