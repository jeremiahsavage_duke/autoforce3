from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Freezer

class StockListView(ListView):
    model=Freezer
    template_name='stock/stock_list.html'

class StockDetailView(DetailView):
    model=Freezer
    
