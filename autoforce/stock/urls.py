from django.conf.urls import include, patterns, url

from .views import StockDetailView, StockListView

urlpatterns=patterns('',
                     url(r'^$',StockListView.as_view(),name='stock-list'),
                     url(r'^(?P<species_pk>\d+)/$',StockDetailView.as_view(),name='stock-detail'),
                     )
