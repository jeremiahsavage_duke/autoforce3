import argparse
import os
import re

parser=argparse.ArgumentParser('')
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-a','--app',required=True)
parser.add_argument('-m','--model',required=True)
parser.add_argument('-c','--childkey',required=True)
parser.add_argument('-p','--parentkey',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
app=args['app']
model=args['model']
childkey=args['childkey']
parentkey=args['parentkey']

#keywords: id parentid
def get_ids(line,childkey,parentkey):
    items_string=re.match(r'.*\[(.*)\].*',line).group(1)
    splititems=items_string.split(';')
    for item in splititems:
        item=item.strip()
        if item.lower().startswith(childkey.lower()):
            childid=item.split(':')[1].replace('\'','').replace(']','').strip()
            childid='a'+str(childid)
        if item.lower().startswith(parentkey.lower()):
            parentid=item.split(':')[1].replace('\'','').replace(']','').strip()
            parentid='a'+str(parentid)
    return childid,parentid

def get_phenotype(line):
    pheno_string=re.match(r'(.*)\[.*\].*',line).group(1)
    pheno_string=pheno_string.replace('#','').replace('\'','\\\'').strip().rstrip(':')
    return pheno_string

def get_set(line,key):
    aset=set()
    items_string=re.match(r'.*\[(.*)\].*',line).group(1).replace('\'','')
    splititems=items_string.split(';')
    for item in splititems:
        item=item.strip()
        if item.lower().startswith(key):
            astring=item.split(':')[1].replace(']','').strip()
            alist=astring.split(',')
            for aitem in alist:
                aset.add(aitem)
    return aset

def doit(infile,outfile,app,model,childkey,parentkey):
    with open(infile,'r') as infile_open:
        infile_lines=infile_open.readlines()

    outfile_open=open(outfile,'w')

    import_string='from ' + app.lower() + '.models import ' + model + ',EOM,HPO,UMLS,FeatureType\nfrom taxonomy.models import Species\nspecies=Species.objects.get(ncbi_id=9696)\n\n'
    outfile_open.write(import_string)


    for infile_line in infile_lines:
        if '' == infile_line.strip() or infile_line.strip()=='END':
            continue
        childid,parentid=get_ids(infile_line,childkey,parentkey)
        phenotype=get_phenotype(infile_line)
        eom_set=get_set(infile_line,'eomids')
        hpo_set=get_set(infile_line,'hpoids')
        umls_set=get_set(infile_line,'umlsids')
        ptype_set=get_set(infile_line,'type')
        if 'a0'==parentid:
            create_string=childid+'=' +model+ '.objects.create(name=' + '\'' +phenotype+ '\'' + ',ncbi_species=species)'
        else:
            create_string=childid+'=' +model+ '.objects.create(name=' + '\'' +phenotype+ '\',parent=' +parentid+ ',ncbi_species=species)'
        outfile_open.write(create_string+'\n')
        if eom_set:
            for eom in eom_set:
                object_string='eom_object=EOM.objects.get(eom=\''+eom+'\')'
                add_string='eom_object.phenotype.add('+childid+')'
                outfile_open.write(object_string+'\n'+add_string+'\n')
        if hpo_set:
            for hpo in hpo_set:
                object_string='hpo_object=HPO.objects.get(hpo=\''+hpo+'\')'
                add_string='hpo_object.phenotype.add('+childid+')'
                outfile_open.write(object_string+'\n'+add_string+'\n')
        if umls_set:
            for umls in umls_set:
                object_string='umls_object=UMLS.objects.get(umls=\''+umls+'\')'
                add_string='umls_object.phenotype.add('+childid+')'
                outfile_open.write(object_string+'\n'+add_string+'\n')
        if ptype_set:
            for ptype in ptype_set:
                object_string='ptype_object=FeatureType.objects.get(featuretype=\''+ptype+'\')'
                add_string='ptype_object.phenotype.add('+childid+')'
                outfile_open.write(object_string+'\n'+add_string+'\n')

    outfile_open.close()

doit(infile,outfile,app,model,childkey,parentkey)
