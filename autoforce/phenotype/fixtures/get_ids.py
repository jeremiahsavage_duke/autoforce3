import argparse
import os
import re

parser=argparse.ArgumentParser('')
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-k','--keyword',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
keyword=args['keyword']

def get_ids(line,keyword):
    items_string=re.match(r'.*\[(.*)\].*',line).group(1)
    splititems=items_string.split(';')
    for item in splititems:
        if keyword.lower() in item.lower():
            ids_string=item.split(':')[1].replace('\'','').replace(']','').strip()
            ids_set=set(ids_string.split(','))
            return ids_set

def doit(infile,outfile,keyword):
    id_set=set()

    with open(infile,'r') as infile_open:
        infile_lines=infile_open.readlines()

    outfile_open=open(outfile,'w')

    for infile_line in infile_lines:
        if keyword.lower() in infile_line.lower():
            new_id_set=get_ids(infile_line,keyword)
            id_set.update(new_id_set)

    id_list=list(id_set)
    id_list.sort()
            
    for a_id in id_list:
        outfile_open.write(a_id+'\n')
    outfile_open.close()

doit(infile,outfile,keyword)
