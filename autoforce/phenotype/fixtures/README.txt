
python ./get_ids.py -i features.txt -o eom.id -k eomid
python ./get_ids.py -i features.txt -o umls.id -k umlsid
python ./get_ids.py -i features.txt -o hpo.id -k hpoid
python ./get_ids.py -i features.txt -o type.id -k type


python ./initial_keys.py -i umls.id -o umls.py -a Phenotype -m UMLS -f umls -d ''
python ./initial_keys.py -i hpo.id -o hpo.py -a Phenotype -m HPO -f hpo -d 1
python ./initial_keys.py -i eom.id -o eom.py -a Phenotype -m EOM -f eom -d ''
python ./initial_keys.py -i type.id -o type.py -a Phenotype -m FeatureType -f featuretype -d ''

python ./initial_pheno.py -i features.txt -o features.py -a Phenotype -m Phenotype -c id -p parent


python ./manage.py shell < phenotype/fixtures/umls.py
python ./manage.py shell < phenotype/fixtures/hpo.py
python ./manage.py shell < phenotype/fixtures/eom.py
python ./manage.py shell < phenotype/fixtures/type.py
python ./manage.py shell < phenotype/fixtures/features.py
