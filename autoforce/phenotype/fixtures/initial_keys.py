import argparse
import os

parser=argparse.ArgumentParser('')
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-a','--app',required=True)
parser.add_argument('-m','--model',required=True)
parser.add_argument('-f','--field',required=True)
parser.add_argument('-d','--doint',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
app=args['app']
model=args['model']
field=args['field']
doint=bool(args['doint'])

def doit(infile,outfile,app,model,field,doint):
    with open(infile,'r') as infile_open:
        infile_lines=infile_open.readlines()

    outfile_open=open(outfile,'w')

    import_string='from ' + app.lower() + '.models import ' + model + '\n\n'
    outfile_open.write(import_string)

    for infile_line in infile_lines:
        if doint:
            create_string=model + '.objects.create(' + field + '=' + str(int(infile_line.strip())) + ')'
        else:
            create_string=model + '.objects.create(' + field + '=' + '\'' +infile_line.strip() + '\'' + ')'
        outfile_open.write(create_string+'\n')

    outfile_open.close()

doit(infile,outfile,app,model,field,doint)
