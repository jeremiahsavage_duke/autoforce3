from django.shortcuts import render, render_to_response
from django.views.generic import DetailView, ListView, UpdateView
from django.template import RequestContext

from .models import Phenotype
from taxonomy.models import Species

class PhenotypeListView(ListView):
    model=Phenotype
    template='phenotype/phenotype_list.html'

    def get_context_data(self,**kwargs):
        context=super(PhenotypeListView,self).get_context_data(**kwargs)
        species_pk=self.kwargs['species_pk']
        ncbi_species=Species.objects.get(pk=species_pk)
        context['phenotype_list']=Phenotype.objects.filter(ncbi_species=ncbi_species)
        context['species_pk']=species_pk
        return context

def zerofill_hpo(hpo_list):
    new_list=list()
    for hpo in hpo_list:
        new_hpo=str(hpo.hpo).zfill(7)
        new_list.append(new_hpo)
    return new_list

class PhenotypeDetailView(DetailView):
    model=Phenotype
    template='phenotype/phenotype_detail.html'
    pk_url_kwarg='phenotype_pk'

    def get_context_data(self,**kwargs):
        context=super(PhenotypeDetailView,self).get_context_data(**kwargs)
        phenotype_pk=self.kwargs['phenotype_pk']
        phenotype=Phenotype.objects.get(pk=phenotype_pk)
        context['phenotype']=phenotype
        species_pk=self.kwargs['species_pk']
        species=Species.objects.get(pk=species_pk)
        context['species']=species
        eom_list=phenotype.eom_set.all()
        hpo_list=phenotype.hpo_set.all()
        hpo_list=zerofill_hpo(hpo_list)
        umls_list=phenotype.umls_set.all()
        context['eom_set']=eom_list
        context['hpo_set']=hpo_list
        context['umls_set']=umls_list
        return context

class PhenotypeUpdateView(UpdateView):
   model=Phenotype
   template='phenotype/phenotype_update.html'

   def get_form_kwargs(self):
       kwargs=super(PhenotypeUpdateView,self).get_form_kwargs()
       #kwargs.update()
       return kwargs

def show_phenotypes(request,**kwargs):
    return render_to_response('phenotype/phenotype_list.html',
                              {'nodes':Phenotype.objects.all()},
                              context_instance=RequestContext(request))
