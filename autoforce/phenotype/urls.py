from django.conf.urls import include, patterns, url

from .views import PhenotypeDetailView, PhenotypeListView, PhenotypeUpdateView, show_phenotypes

urlpatterns=patterns('',
                     url(r'^$',PhenotypeListView.as_view(),name='phenotype-list'),
                     url(r'^(?P<phenotype_pk>\d+)/$',PhenotypeDetailView.as_view(),name='phenotype-detail'),
                     url(r'^$',PhenotypeUpdateView.as_view(),name='phenotype-update'),
                     )
