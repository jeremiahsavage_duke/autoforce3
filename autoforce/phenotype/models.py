from django.db import models
from django.core.urlresolvers import reverse

from mptt.models import MPTTModel, TreeForeignKey

from taxonomy.models import Species
#from patient.models import Patient

class Phenotype(MPTTModel):
    name=models.CharField(max_length=255,unique=False)
    parent=TreeForeignKey('self',null=True,blank=True,related_name='children',db_index=True)
    ncbi_species=models.ForeignKey(Species)

    class MPTTMeta:
        order_insertion_by=['name']

    def __str__(self):
        return self.name


'''
    def get_absolute_url(self):
        patient_pk=self.kwargs['patient_pk']
        patient=Patient.objects.get(pk=patient_pk)
        return reverse('data:family:patient:patient-detail',kwargs={'pk':patient_pk,})
'''


#anu, group, a, other, birl
class FeatureType(models.Model):
    featuretype=models.CharField(max_length=255)
    phenotype=models.ManyToManyField(Phenotype)
    def __str__(self):
        return self.name

class UMLS(models.Model):
    umls=models.CharField(max_length=255)
    phenotype=models.ManyToManyField(Phenotype)
    def __str__(self):
        return self.umls

class HPO(models.Model):
    hpo=models.IntegerField()
    phenotype=models.ManyToManyField(Phenotype)
    def __str__(self):
        return self.hpo

class EOM(models.Model):
    eom=models.CharField(max_length=255)
    phenotype=models.ManyToManyField(Phenotype)
    def __str__(self):
        return self.eom

