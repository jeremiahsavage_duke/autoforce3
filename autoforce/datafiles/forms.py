from os.path import join

from django.forms import ModelForm, HiddenInput, FilePathField
from django.core.files.storage import FileSystemStorage

from .models import PatientData, DataFile
#from patient.models import Patient
from family.models import Family

class PatientDataFileForm(ModelForm):
    def __init__(self,family_pk,*args,**kwargs):
        super(PatientDataFileForm,self).__init__(*args,**kwargs)
        fs=FileSystemStorage()
        familyname=Family.objects.get(pk=family_pk)
        filepath=join(fs.location,str(familyname),'datafiles/')
        self.fields['filepath']=FilePathField(path=filepath,recursive=False)

    class Meta:
        model=DataFile
        fields='__all__'
        widgets={'patient':HiddenInput(),'slug':HiddenInput()}
