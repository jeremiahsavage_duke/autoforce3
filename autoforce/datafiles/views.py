from os.path import abspath, dirname, join, basename

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render
from django.views.generic import CreateView, DeleteView, DetailView, ListView
from django.core.files.storage import FileSystemStorage

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from family.models import Family
from patient.models import Patient
from .models import PatientData, DataFile
from .forms import PatientDataFileForm


from autoforce import settings

from django_downloadview.views import StorageDownloadView
#app_dir=dirname(abspath(__file__))
#fixtures_dir=join(app_dir,'fixtures')
media_root=settings.MEDIA_ROOT
fixtures_storage=FileSystemStorage(location=media_root)
download_fixture_from_storage=StorageDownloadView.as_view(storage=fixtures_storage)

class DataFileStorageDownloadView(StorageDownloadView):
    model=DataFile
    pk_url_kwarg='datafile_pk'
    
    def get_path(self):
        filename=DataFile.objects.get(pk=self.kwargs['datafiledata_pk']).slug
        patient_pk=DataFile.objects.get(pk=self.kwargs['datafiledata_pk']).patient.pk
        familyname=Patient.objects.get(pk=patient_pk).family.alias
        self.path=familyname+'/datafiles/'+filename
        return super(DataFileStorageDownloadView,self).get_path()


class PatientDataFileCreateView(CreateView):
    model=DataFile
    form_class=PatientDataFileForm
    #context_object_name='patientdata'

    def form_valid(self,form):
        self.object=form.save()
        self.object.slug=basename(self.object.filepath)
        self.object.save()
        return super(PatientDataCreateView,self).form_valid(form)

    def get_context_data(self,**kwargs):
        context=super(PatientDataCreateView,self).get_context_data(**kwargs)
        context['patient']=Patient.objects.get(pk=self.kwargs.get('patient_pk'))
        context['family']=Family.objects.get(pk=self.kwargs.get('family_pk'))
        return context

    def get_form_kwargs(self):
        kwargs=super(PatientDataCreateView,self).get_form_kwargs()
        kwargs['family_pk']=self.kwargs.get('family_pk')
        #kwargs['patient_pk']=self.kwargs.get('patient_pk')
        return kwargs

    def get_initial(self,*args,**kwargs):
        patient=get_object_or_404(Patient,pk=self.kwargs.get('patient_pk'))
        return {'patient':patient,}

    def get_success_url(self):
        return reverse('data:family:patient:patient-detail',kwargs={'patient_pk':self.kwargs['patient_pk'], 'family_pk':self.kwargs['family_pk']})

    @method_decorator(login_required)
    def dispatch(self,*args,**kwargs):
        return super(PatientDataCreateView,self).dispatch(*args,**kwargs)

class DataFileListView(ListView):
    model=DataFile
    context_object_name='patientdata_list'

    def get_context_data(self,*args,**kwargs):
        context=super(DataFileListView,self).get_context_data(**kwargs)
        context['family']=Family.objects.filter(pk=self.kwargs.get('family_pk'))
        context['patient']=Patient.objects.get(pk=self.kwargs.get('patient_pk'))
        context['datafile']=DataFile.objects.all()
        return context


class DataFileDeleteView(DeleteView):
    model=PatientData

    def get_success_url(self,*args,**kwargs):
        return reverse('family:patient:patient-detail',kwargs={'patient_pk':self.kwargs['patient_pk'],'family_pk':self.kwargs['family_pk']})


class DataFileDetailView(DetailView):
    model=DataFile
    pk_url_kwarg='datafile_pk'

    def get_context_data(self,*args,**kwargs):
        context=super(DataFileDetailView,self).get_context_data(**kwargs)
        context['family']=Family.objects.filter(pk=self.kwargs.get('family_pk'))
        context['patient']=Patient.objects.get(pk=self.kwargs.get('patient_pk'))
        #context['patientdata']=PatientData.objects.all()
        return context
