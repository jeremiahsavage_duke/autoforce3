from os.path import join
from django.db import models
from django.core.files.storage import FileSystemStorage

from patient.models import Patient
from family.models import Family

class PatientData(models.Model):
    slug=models.CharField(max_length=255,blank=True)
    filepath=models.FilePathField(max_length=255,blank=True,null=True,recursive=False)
    patient=models.ForeignKey(Patient)

    def __str__(self):
        return self.slug

class DataFile(models.Model):
    slug=models.CharField(max_length=255,blank=True)
    filepath=models.FilePathField(max_length=255,blank=True,null=True,recursive=False)
    patient=models.ManyToManyField(Patient)
    family=models.ManyToManyField(Family)

    def __str__(self):
        return self.slug
