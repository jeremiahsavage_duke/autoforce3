from django.conf.urls import include, patterns, url

from .views import PatientDataFileCreateView, DataFileDeleteView, DataFileDetailView, DataFileListView, DataFileStorageDownloadView

urlpatterns=patterns('',
                     url(r'^$',DataFileListView.as_view(),name='list'),
                     url(r'^create/$',PatientDataFileCreateView.as_view(),name='create'),
                     url(r'^delete/$',DataFileDeleteView.as_view(),name='delete'),
                     url(r'^detail/(?P<datafile_pk>\d+)/$',DataFileDetailView.as_view(),name='detail'),
                     url(r'^download/(?P<datafile_pk>\d+)/$',DataFileStorageDownloadView.as_view(),name='datafile_from_storage'),
                 )
