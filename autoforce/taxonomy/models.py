from django.db import models

class Genus(models.Model):
    name=models.CharField(max_length=255)
    ncbi_id=models.IntegerField()

    def __str__(self):
        return self.name

class Species(models.Model):
    genus=models.ForeignKey(Genus)
    name=models.CharField(max_length=255)
    common_name=models.CharField(max_length=255)
    ncbi_id=models.IntegerField()

    def __str__(self):
        return self.name
