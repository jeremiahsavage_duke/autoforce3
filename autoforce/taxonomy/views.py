from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.generic import DetailView, ListView, FormView

from .models import Genus, Species
from .forms import QueryGeneForm, QueryPhenotypeForm

class TaxonomyListView(ListView):
    model=Species

class TaxonomyDetailView(DetailView):
    model=Species
    pk_url_kwarg='species_pk'

class QueryGeneView(FormView):
    form_class=QueryGeneForm
    pk_url_kwarg='species_pk'
    template_name='taxonomy/query_gene.html'
    success_url=reverse_lazy('data:patient:patient-list')

class QueryPhenotypeView(FormView):
    form_class=QueryPhenotypeForm
    pk_url_kwarg='species_pk'
    template_name='taxonomy/query_phenotype.html'
    success_url=reverse_lazy('data:patient:patient-list')
