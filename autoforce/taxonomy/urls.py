from django.conf.urls import include, patterns, url

from .views import TaxonomyDetailView, TaxonomyListView, QueryGeneView, QueryPhenotypeView

urlpatterns=patterns('',
                     url(r'^$',TaxonomyListView.as_view(),name='taxonomy-list'),
                     url(r'^(?P<species_pk>\d+)/$',TaxonomyDetailView.as_view(),name='taxonomy-detail'),
                     url(r'^(?P<species_pk>\d+)/gene/',include('gene.urls',namespace='gene')),
                     url(r'^(?P<species_pk>\d+)/phenotype/',include('phenotype.urls',namespace='phenotype')),
                     url(r'^(?P<species_pk>\d+)/query-gene/$',QueryGeneView.as_view(),name='query-gene'),
                     url(r'^(?P<species_pk>\d+)/query-phenotype/$',QueryPhenotypeView.as_view(),name='query-phenotype'),
                     )
