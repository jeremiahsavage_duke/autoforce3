from django import forms

from phenotype.models import Phenotype
from gene.models import Gene

class QueryGeneForm(forms.Form):
    phenotype=Phenotype.objects.all()

class QueryPhenotypeForm(forms.Form):
    gene=Gene.objects.all()
